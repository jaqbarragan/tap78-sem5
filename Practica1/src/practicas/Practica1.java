
package practicas;

import com.sun.corba.se.impl.encoding.CodeSetConversion;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;


public class Practica1 implements ActionListener{
    JFrame ventana;
    JLabel lblNombre;
    JTextField tfNombre;
    JButton btnSaludar;
    
    
    public static void main(String[] args) {
        
        Practica1 app= new Practica1();
        app.run();
    }
    
    void run(){
       ventana = new JFrame("Practica 1");
       ventana.setLayout(new FlowLayout());//Propiedad del JFrame que actua como una plantila.
       ventana.setSize(300, 200); //*Primero le damos un tamaño
        
       lblNombre = new JLabel("Escriba su nombre");//Creamos una etiqueta en donde dice que escriba su nombre.
       tfNombre = new JTextField(20);//Creamos un campo daa{e texto con limite de 30 caracteres
       btnSaludar = new JButton("Saludar");//Creamos un boton que contenga saludar
        //Primera forma
       btnSaludar.addActionListener(new ActionListener() {//Agregamos el listener o btnSaludar.addActionListener(this);
       
       
        @Override
           public void actionPerformed(ActionEvent e) {

               JOptionPane.showMessageDialog(null, "Hola "+tfNombre.getText(), "Mensaje", 2);// Al pulsar el boton saludar aparecera un mensaje emergente 
        }
        
    });
       
       //Segunda forma
       //btnSaludar.addActionListener(this);
        
       ventana.add(lblNombre);//Agregamos la etiqueta a la plantilla -> ventana 
       ventana.add(tfNombre);//Agregamos el campo de texto a la plantilla -> ventana
       ventana.add(btnSaludar);//Agregamos el boton saludar a la plantilla -> ventana
       
       ventana.setVisible(true);//*Segundo hacerlo visible
       ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//Cierra la ventana y termina el proceso.
    }
     
    @Override
    public void actionPerformed(ActionEvent ae) {
         JOptionPane.showMessageDialog(ventana, "Hola "+tfNombre.getText());
    
       
        }
        

    
}
